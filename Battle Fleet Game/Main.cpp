/*
// Daniel Turner
// 5/12/2011
// Semester Project
// Battle Fleet Game
*/

// Includes

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Fleet Struct

typedef struct {

		int submarine;
		int destroyer;
		int cruiser;
		int battleship;
		int carrier;
		int x;
		int y;
		int counter;
		bool up;
		bool down;
		bool left;
		bool right;

	} FLEET;

// Function Prototypes

bool check_Bounds(int x, int y);
bool check_Conflict(int x, int y, char grid[10][10]);
void displayGrid(char grid[10][10]);
void generateGrid(char grid[10][10]);
void findShip(char grid[10][10], char enemyGrid[10][10], bool* shipFound, FLEET* myFleet);
void findShipMedium(char grid[10][10], char enemyGrid[10][10], bool* shipFound, FLEET* myFleet);
void attackShipMedium(char grid[10][10], char enemyGrid[10][10], FLEET* myFleet, int* counter, bool* shipFound, bool* PBF_Hit);
void attackShip(char grid[10][10], char enemyGrid[10][10], FLEET* myFleet, int* counter, bool* shipFound);
bool PVPMode(int* choice);
bool PVCMode(int* choice);
bool CVCMode(int* choice);
void MainMenu(int* choice);
void deploySubmarine(char grid[10][10]);
void deployDestroyer(char grid[10][10]);
void deployCruiser(char grid[10][10]);
void deployBattleship(char grid[10][10]);
void deployCarrier(char grid[10][10]);
void deployFleet(char grid[10][10]);
bool easyMode(int* choice);
bool intermediateMode(int* choice);
bool hardMode(int* choice);
void displayMsg(void);

// Main

int main() {

	int choice = 0;
	bool playAgain = true;

	while(playAgain == true) {

		MainMenu(&choice);

		switch(choice) {

			case 1 : {

				playAgain = PVPMode(&choice);
				break;
			}

			case 2 : {

				playAgain = PVCMode(&choice);
				break;
			}

			case 3 : {

				playAgain = CVCMode(&choice);
				break;
			}

			default : {

				playAgain = false;
				break;
			}
		}
	}
	printf("\n\nEXITING GAME...");

	return 0;
}

// This function makes sure that the user inputs a coordinate within the bounds of the grid.
// It returns true if the user enters a coordinate that is out of bounds.

bool check_Bounds(int x, int y) {

	if((x > 10 || x < 0) || (y > 10 || y < 0))
		return true;

	else
		return false;
}

// This function makes sure that the user doesn't enter a coordinate that has already been chosen by the user.
// It returns true if a conflict is found.

bool check_Conflict(int x, int y, char grid[10][10]) {

	if(grid[y][x] != NULL)
		return true;

	else
		return false;
}

// This function displays the grid.

void displayGrid(char grid[10][10]) {

	printf("\n   0   1   2   3   4   5   6   7   8   9\n");
	printf("  ---------------------------------------\n");

	for(int i = 0; i < 10; i++) {
		printf("%d|" , i);
		for(int c = 0; c < 10; c++) {
			printf(" %c |", grid[i][c]);
		}
		printf("\n");
		printf("  ---------------------------------------\n");
	}

	return;
}

// This function generates a grid for the computer.

void generateGrid(char grid[10][10]) {

	int x = 0;
	int y = 0;

	int direction = rand() % 2;

	switch(direction) {

	// Horizontal Placemant

		case 0 : {

			x = rand() % 9;
			y = rand() % 10;
			grid[y][x] = '1';
			grid[y][x+1] = '1';
			break;
		}
	
	// Vertical Placement

		case 1 : {

			x = rand() % 10;
			y = rand() % 9;
			grid[y][x] = '1';
			grid[y+1][x] = '1';
			break;
		}
	}

	// SUBMARINE END

	direction = rand() % 2;

	switch(direction) {

		case 0 : {

			x = rand() % 8;
			y = rand() % 10;

			while(grid[y][x] != NULL || grid[y][x+1] != NULL || grid[y][x+2] != NULL) {

				x = rand() % 8;
				y = rand() % 10;
			}

			grid[y][x] = '2';
			grid[y][x+1] = '2';
			grid[y][x+2] = '2';
			break;

		}

		case 1 : {

			x = rand() % 10;
			y = rand() % 8;

			while(grid[y][x] != NULL || grid[y+1][x] != NULL || grid[y+2][x] != NULL) {

				x = rand() % 10;
				y = rand() % 8;
			}

			grid[y][x] = '2';
			grid[y+1][x] = '2';
			grid[y+2][x] = '2';
			break;
		}
	}

	// DESTROYER END

	direction = rand() % 2;

	switch(direction) {

		case 0 : {

			x = rand() % 8;
			y = rand() % 10;

			while(grid[y][x] != NULL || grid[y][x+1] != NULL || grid[y][x+2] != NULL) {

				x = rand() % 8;
				y = rand() % 10;
			}

			grid[y][x] = '3';
			grid[y][x+1] = '3';
			grid[y][x+2] = '3';
			break;

		}

		case 1 : {

			x = rand() % 10;
			y = rand() % 8;

			while(grid[y][x] != NULL || grid[y+1][x] != NULL || grid[y+2][x] != NULL) {

				x = rand() % 10;
				y = rand() % 8;
			}

			grid[y][x] = '3';
			grid[y+1][x] = '3';
			grid[y+2][x] = '3';
			break;
		}
	}

	// CRUISER END

	direction = rand() % 2;

	switch(direction) {

		case 0 : {

			x = rand() % 7;
			y = rand() % 10;

			while(grid[y][x] != NULL || grid[y][x+1] != NULL || grid[y][x+2] != NULL || grid[y][x+3] != NULL) {

				x = rand() % 7;
				y = rand() % 10;
			}

			grid[y][x] = '4';
			grid[y][x+1] = '4';
			grid[y][x+2] = '4';
			grid[y][x+3] = '4';
			break;

		}

		case 1 : {

			x = rand() % 10;
			y = rand() % 7;

			while(grid[y][x] != NULL || grid[y+1][x] != NULL || grid[y+2][x] != NULL || grid[y+3][x] != NULL) {

				x = rand() % 10;
				y = rand() % 7;
			}

			grid[y][x] = '4';
			grid[y+1][x] = '4';
			grid[y+2][x] = '4';
			grid[y+3][x] = '4';
			break;
		}
	}

	// BATTLESHIP END

	direction = rand() % 2;

	switch(direction) {

		case 0 : {

			x = rand() % 6;
			y = rand() % 10;

			while(grid[y][x] != NULL || grid[y][x+1] != NULL || grid[y][x+2] != NULL || grid[y][x+3] != NULL || grid[y][x+4] != NULL) {

				x = rand() % 6;
				y = rand() % 10;
			}

			grid[y][x] = '5';
			grid[y][x+1] = '5';
			grid[y][x+2] = '5';
			grid[y][x+3] = '5';
			grid[y][x+4] = '5';
			break;

		}

		case 1 : {

			x = rand() % 10;
			y = rand() % 6;

			while(grid[y][x] != NULL || grid[y+1][x] != NULL || grid[y+2][x] != NULL || grid[y+3][x] != NULL || grid[y+4][x] != NULL) {

				x = rand() % 10;
				y = rand() % 6;
			}

			grid[y][x] = '5';
			grid[y+1][x] = '5';
			grid[y+2][x] = '5';
			grid[y+3][x] = '5';
			grid[y+4][x] = '5';
			break;
		}
	}


	// CARRIER END

	return;
}

// This function attempts to find the direction of a ship after a hit.

void findShip(char grid[10][10], char enemyGrid[10][10], bool* shipFound, FLEET* myFleet) {

		int direction = 0;
		int horizontalDirection = 0;
		int verticalDirection = 0;

		direction = rand() % 2;	   // Chooses a random direction (horizontal or vertical)

		if(direction == 0) {      // Horizontal

			horizontalDirection = rand() % 2;

			if(horizontalDirection == 0) {

				if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

					if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

						(myFleet->x)++;
						(myFleet->counter)++;
						(myFleet->right) = true;
						(*shipFound) = true;
					}

					else

						grid[myFleet->y][(myFleet->x) + 1] = 'O';


				}

				else {

					if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

						if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {
							
							(myFleet->x)--;
							(myFleet->counter)++;	
							(myFleet->left) = true;
							(*shipFound) = true;
						}

						else 

							grid[myFleet->y][(myFleet->x) - 1] = 'O';
			
					}

					else {

						verticalDirection = rand() % 2;

						if(verticalDirection == 0) {

							if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {

								if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

									(myFleet->y)++;
									(myFleet->counter)++;
									(myFleet->down) = true;
									(*shipFound) = true;
								}

								else

									grid[(myFleet->y) + 1][myFleet->x] = 'O';

							}

							else {

								if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {

									if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

										(myFleet->y)--;
										(myFleet->counter)++;
										(myFleet->up) = true;
										(*shipFound) = true;

									}
									else

										grid[(myFleet->y) - 1][myFleet->x] = 'O';
								}
							}

						}

						else {

							if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {

								if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

									(myFleet->y)--;
									(myFleet->counter)++;
									(myFleet->up) = true;
									(*shipFound) = true;

								}
								else

									grid[(myFleet->y) - 1][myFleet->x] = 'O';

								}

							else {

								
								if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {
	
									if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

										(myFleet->y)++;
										(myFleet->counter)++;
										(myFleet->down) = true;
										(*shipFound) = true;
									}

									else

										grid[(myFleet->y) + 1][myFleet->x] = 'O';

								}
							 }
						  }
						}
					 }
				}	
			
				

			else {  

				if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

					if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

						(myFleet->x)--;
						(myFleet->counter)++;
						(myFleet->left) = true;
						(*shipFound) = true;
					}

					else

						grid[myFleet->y][(myFleet->x) - 1] = 'O';


				}

			

				else {

					if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

						if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

							(myFleet->x)++;
							(myFleet->counter)++;
							(myFleet->right) = true;
							(*shipFound) = true;
						}

						else

							grid[myFleet->y][(myFleet->x) + 1] = 'O';

					}

					else {

						verticalDirection = rand() % 2;

						if(verticalDirection == 0) {

							if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {

								if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

									(myFleet->y)++;
									(myFleet->counter)++;
									(myFleet->down) = true;
									(*shipFound) = true;
								}

								else

									grid[(myFleet->y) + 1][myFleet->x] = 'O';

							}

							else {

								if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {

									if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

										(myFleet->y)--;
										(myFleet->counter)++;
										(myFleet->up) = true;
										(*shipFound) = true;
									}

								else

									grid[(myFleet->y) - 1][myFleet->x] = 'O';

								}
							}
						}

						else {

							if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {

								if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

									(myFleet->y)--;
									(myFleet->counter)++;
									(myFleet->up) = true;
									(*shipFound) = true;
								}

								else

									grid[(myFleet->y) - 1][myFleet->x] = 'O';
							}

							else {

								if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {

									if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

										(myFleet->y)++;
										(myFleet->counter)++;
										(myFleet->down) = true;
										(*shipFound) = true;
									}

									else
		
										grid[(myFleet->y) + 1][myFleet->x] = 'O';
								}
							}
						}
					}
				}
			}
		}
		

		// HORIZONTAL END

		else {			// Vertical

			verticalDirection = rand() % 2;

			if(verticalDirection == 0) {

				if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {

					if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

						(myFleet->y)++;
						(myFleet->counter)++;
						(myFleet->down) = true;
						(*shipFound) = true;
					}

					else

						grid[(myFleet->y) + 1][myFleet->x] = 'O';

				}

				else {

					if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {
						
						if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

							(myFleet->y)--;
							(myFleet->counter)++;
							(myFleet->up) = true;
							(*shipFound) = true;
						}

						else

							grid[(myFleet->y) - 1][myFleet->x] = 'O';

					}

					else {

						horizontalDirection = rand() % 2;

						if(horizontalDirection == 0) {

							if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

								if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

									(myFleet->x)++;
									(myFleet->counter)++;
									(myFleet->right) = true;
									(*shipFound) = true;
								}

								else

									grid[myFleet->y][(myFleet->x) + 1] = 'O';
							}

							else {

								if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

									if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

										(myFleet->x)--;
										(myFleet->counter)++;
										(myFleet->left) = true;
										(*shipFound) = true;
									}

									else

										grid[myFleet->y][(myFleet->x) - 1] = 'O';
								}
							}
						}

						else {

							if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

								if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

									(myFleet->x)--;
									(myFleet->counter)++;
									(myFleet->left) = true;
									(*shipFound) = true;
								}

								else

									grid[myFleet->y][(myFleet->x) - 1] = 'O';
							}

							else {

								if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

									if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

										(myFleet->x)++;
										(myFleet->counter)++;
										(myFleet->right) = true;
										(*shipFound) = true;
									}

									else

										grid[myFleet->y][(myFleet->x) + 1] = 'O';
								}
							}
						}
					}
				}
			}

			else {

				if((myFleet->y) - 1 >= 0 && grid[(myFleet->y) - 1][myFleet->x] != 'X' && grid[(myFleet->y) - 1][myFleet->x] != 'O') {

					if(enemyGrid[(myFleet->y) - 1][myFleet->x] != NULL) {

						(myFleet->y)--;
						(myFleet->counter)++;
						(myFleet->up) = true;
						(*shipFound) = true;
					}

					else

						grid[(myFleet->y) - 1][myFleet->x] = 'O';

				}

				else {

					if((myFleet->y) + 1 < 10 && grid[(myFleet->y) + 1][myFleet->x] != 'X' && grid[(myFleet->y) + 1][myFleet->x] != 'O') {

						if(enemyGrid[(myFleet->y) + 1][myFleet->x] != NULL) {

							(myFleet->y)++;
							(myFleet->counter)++;
							(myFleet->down) = true;
							(*shipFound) = true;
						}

						else

							grid[(myFleet->y) + 1][myFleet->x] = 'O';

					}

					else {

						horizontalDirection = rand() % 2;

						if(horizontalDirection == 0) {

							if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {
					
								if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

									(myFleet->x)++;
									(myFleet->counter)++;
									(myFleet->right) = true;
									(*shipFound) = true;
								}

								else

									grid[myFleet->y][(myFleet->x) + 1] = 'O';

							}

							else {

									if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

										if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

											(myFleet->x)--;
											(myFleet->counter)++;
											(myFleet->left) = true;
											(*shipFound) = true;
										}

										else

											grid[myFleet->y][(myFleet->x) - 1] = 'O';
									}
							}
						}

						else {

							if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

								if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

									(myFleet->x)--;
									(myFleet->counter)++;
									(myFleet->left) = true;
									(*shipFound) = true;
								}

								else

									grid[myFleet->y][(myFleet->x) - 1] = 'O';
							}

							else {

								
								if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {
					
									if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

										(myFleet->x)++;
										(myFleet->counter)++;
										(myFleet->right) = true;
										(*shipFound) = true;
									}
	
									else

										grid[myFleet->y][(myFleet->x) + 1] = 'O';
								}
							}
						}
					}
				}
			}
		}

	return;
}

// After the direction of a ship has been found by findShip(), this function stays within bounds to sink the ship.

void attackShip(char grid[10][10], char enemyGrid[10][10], FLEET* myFleet, int* counter, bool* shipFound) {

	if(*counter < 2) {

		if((myFleet->right) == true) {

			if(grid[myFleet->y][(myFleet->x) + 1] == 'O' || grid[myFleet->y][myFleet->x] == 'O' || grid[myFleet->y][(myFleet->x) + 1] == 'X' || (myFleet->x) + 1 > 9) {

				if((myFleet->x - (myFleet->counter + 1)) > 0) {

					myFleet->x = myFleet->x - ++(myFleet->counter);
					myFleet->left = true;
					myFleet->right = false;
					(*counter)++;
				}

				else {

					*shipFound = false;
					*counter = 0;
					myFleet->right = false;
					(myFleet->x)--;
				}
			}

			else {

				myFleet->x = ++(myFleet->x);
				(myFleet->counter)++;
			}
		}

		else {

			if((myFleet->left) == true) {

				if(grid[myFleet->y][(myFleet->x) - 1] == 'O' || grid[myFleet->y][myFleet->x] == 'O' || grid[myFleet->y][(myFleet->x) - 1] == 'X' || (myFleet->x) - 1 < 0) {

					if((myFleet->x + (myFleet->counter + 1)) < 10) {

						myFleet->x = myFleet->x + ++(myFleet->counter);
						myFleet->right = true;
						myFleet->left = false;
						(*counter)++;
					}

					else {

						*shipFound = false;
						*counter = 0;
						myFleet->left = false;
						(myFleet->x)++;
					}
				}

				else {

					myFleet->x = --(myFleet->x);
					(myFleet->counter)++;
				}
			}

			else {

				if((myFleet->down) == true) {

					if(grid[(myFleet->y) + 1][myFleet->x] == 'O' || grid[myFleet->y][myFleet->x] == 'O' || grid[(myFleet->y) + 1][myFleet->x] == 'X' || (myFleet->y) + 1 > 9) {

						if((myFleet->y - (myFleet->counter + 1)) > 0) {

							myFleet->y = myFleet->y - ++(myFleet->counter);
							myFleet->up = true;
							myFleet->down = false;
							(*counter)++;
						}

						else {

							*shipFound = false;
							*counter = 0;
							myFleet->down = false;
							(myFleet->y)--;
						}
					}

					else {

						myFleet->y = ++(myFleet->y);
						(myFleet->counter)++;
					}
				}

				else {

					if((myFleet->up) == true) {

						if(grid[(myFleet->y) - 1][myFleet->x] == 'O'  || grid[myFleet->y][myFleet->x] == 'O'|| grid[(myFleet->y) - 1][myFleet->x] == 'X' || (myFleet->y) - 1 < 0) {

							if((myFleet->y + (myFleet->counter + 1)) < 10) {

								myFleet->y = myFleet->y + ++(myFleet->counter);
								myFleet->down = true;
								myFleet->up = false;
								(*counter)++;
							}

							else {

								*shipFound = false;
								*counter = 0;
								myFleet->up = false;
								(myFleet->y)++;
							}
						}

						else {

							myFleet->y = --(myFleet->y);
							(myFleet->counter)++;
						}
					}
				}
			}
		}
	}


	else {

		*shipFound = false;
		*counter = 0;
	}

	return;
}

// Driver code for the Computer VS Computer Mode.

bool CVCMode(int*  choice) {

	FLEET PBF = {2,3,3,4,5};
	FLEET IRF = {2,3,3,4,5};

	char PBF_Grid[10][10] = {NULL};
	char IRF_Grid[10][10] = {NULL}; 
	char PBF_VisibleGrid[10][10] = {NULL};
	char IRF_VisibleGrid[10][10] = {NULL};

	bool PBF_Turn = true;
	bool GameOver = false;
	bool IRF_hit = false;
	bool IRF_ShipFound = false;
	bool PBF_ShipFound = false;
	bool PBF_hit = false;
	bool PBF_Wins;
	int PBF_Counter = 0;
	int IRF_Counter = 0;

	srand(time(NULL));
	
	generateGrid(PBF_Grid);
	generateGrid(IRF_Grid);

	while(GameOver == false) {

		if(PBF_Turn == true) {

			while(PBF_Turn == true && GameOver == false) {

				if(IRF_hit == false && IRF_ShipFound == false) {

					PBF.x = rand() % 10;
					PBF.y = rand() % 10;
				}

				if(PBF_VisibleGrid[PBF.y][PBF.x] == 'X' || PBF_VisibleGrid[PBF.y][PBF.x] == 'O') 

					PBF_Turn = true;


				if(IRF_ShipFound == true) {

					attackShip(PBF_VisibleGrid, IRF_Grid, &PBF, &PBF_Counter, &IRF_ShipFound);
				}

				if(IRF_hit == true && IRF_ShipFound == false) {

					findShip(PBF_VisibleGrid, IRF_Grid, &IRF_ShipFound, &PBF);
					PBF_Turn = false;
				}

				if(IRF_Grid[PBF.y][PBF.x] == NULL && PBF_VisibleGrid[PBF.y][PBF.x] != 'O') {

					PBF_VisibleGrid[PBF.y][PBF.x] = 'O';
					PBF_Turn = false;

				}

				else {

					if(IRF_Grid[PBF.y][PBF.x] != NULL && PBF_VisibleGrid[PBF.y][PBF.x] != 'X') {

						if(IRF_Grid[PBF.y][PBF.x] == '1') {

							(IRF.submarine)--;	
							printf("\nIRF GOT HIT!\n");
							IRF_hit = true;

							if(IRF.submarine == 0) {

								IRF_ShipFound = false;
								IRF_hit = false;
								PBF.up = false;
								PBF.down = false;
								PBF.right = false;
								PBF.left = false;
								PBF.counter = 0;
								PBF_Counter = 0;
								printf("\nIRF SUBMARINE HAS BEEN SUNK!\n");
							}

						}
	
						else {		

							if(IRF_Grid[PBF.y][PBF.x] == '2') {

								(IRF.destroyer)--;
								printf("\nIRF GOT HIT!\n");
								IRF_hit = true;

								if(IRF.destroyer == 0) {
	
									IRF_ShipFound = false;
									IRF_hit = false;
									PBF.up = false;
									PBF.down = false;
									PBF.right = false;
									PBF.left = false;
									PBF.counter = 0;
									PBF_Counter = 0;
									printf("\nIRF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(IRF_Grid[PBF.y][PBF.x] == '3') {
							
									(IRF.cruiser)--;
									printf("\nIRF GOT HIT!\n");
									IRF_hit = true;

									if(IRF.cruiser == 0) {

										IRF_ShipFound = false;
										IRF_hit = false;
										PBF.up = false;
										PBF.down = false;
										PBF.right = false;
										PBF.left = false;
										PBF.counter = 0;
										PBF_Counter = 0;
										printf("\nIRF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(IRF_Grid[PBF.y][PBF.x] == '4') {

										(IRF.battleship)--;
										printf("\nIRF GOT HIT!\n");
										IRF_hit = true;

										if(IRF.battleship == 0) {
	
											IRF_ShipFound = false;
											IRF_hit = false;
											PBF.up = false;
											PBF.down = false;
											PBF.right = false;
											PBF.left = false;
											PBF.counter = 0;
											PBF_Counter = 0;
											printf("\nIRF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(IRF.carrier)--;
										printf("\nIRF GOT HIT!\n");
										IRF_hit = true;

										if(IRF.carrier == 0) {

											IRF_ShipFound = false;
											IRF_hit = false;
											PBF.up = false;
											PBF.down = false;
											PBF.right = false;
											PBF.left = false;
											PBF.counter = 0;
											PBF_Counter = 0;
											printf("\nIRF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						PBF_VisibleGrid[PBF.y][PBF.x] = 'X';
						PBF_Turn = true;

						printf("\nPBF WENT\n");
						displayGrid(PBF_VisibleGrid);

						if(IRF.submarine == 0 && IRF.destroyer == 0 && IRF.cruiser == 0 && IRF.battleship == 0 && IRF.carrier == 0) {

							PBF_Wins = true;
							GameOver = true;
						}	
					}
				}
			}

			printf("\nPBF WENT\n");
			displayGrid(PBF_VisibleGrid);
		}

		else {

			while(PBF_Turn == false && GameOver == false) {

				if(PBF_hit == false && PBF_ShipFound == false)  {

					IRF.x = rand() % 10;
					IRF.y = rand() % 10;
				}

				if(IRF_VisibleGrid[IRF.y][IRF.x] == 'O' || IRF_VisibleGrid[IRF.y][IRF.x] == 'X')

					PBF_Turn = false;


				if(PBF_ShipFound == true) {

					attackShip(IRF_VisibleGrid, PBF_Grid, &IRF, &IRF_Counter, &PBF_ShipFound);
				}

				if(PBF_hit == true && PBF_ShipFound == false) {

					findShip(IRF_VisibleGrid, PBF_Grid, &PBF_ShipFound, &IRF);
					PBF_Turn = true;

				}	

				if(PBF_Grid[IRF.y][IRF.x] == NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'O') {

					IRF_VisibleGrid[IRF.y][IRF.x] = 'O';
					PBF_Turn = true;
				}

				else {

					if(PBF_Grid[IRF.y][IRF.x] != NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'X') {

						if(PBF_Grid[IRF.y][IRF.x] == '1') {

							(PBF.submarine)--;	
							printf("\nPBF GOT HIT\n");
							PBF_hit = true;

							if(PBF.submarine == 0) {

								PBF_ShipFound = false;
								PBF_hit = false;
								IRF.up = false;
								IRF.down = false;
								IRF.right = false;
								IRF.left = false;
								IRF.counter = 0;
								IRF_Counter = 0;
								printf("\nPBF SUBMARINE HAS BEEN SUNK!\n");
							}
						}

						else {		

							if(PBF_Grid[IRF.y][IRF.x] == '2') {

								(PBF.destroyer)--;
								printf("\nPBF GOT HIT\n");
								PBF_hit = true;

								if(PBF.destroyer == 0) {

									PBF_ShipFound = false;
									PBF_hit = false;
									IRF.up = false;
									IRF.down = false;
									IRF.right = false;
									IRF.left = false;
									IRF.counter = 0;
									IRF_Counter = 0;
									printf("\nPBF DESTROYER HAS BEEN SUNK!\n");
								}
							}

							else {

								if(PBF_Grid[IRF.y][IRF.x] == '3') {
							
									(PBF.cruiser)--;
									printf("\nPBF GOT HIT\n");
									PBF_hit = true;

									if(PBF.cruiser == 0) {

										PBF_ShipFound = false;
										PBF_hit = false;
										IRF.up = false;
										IRF.down = false;
										IRF.right = false;
										IRF.left = false;
										IRF.counter = 0;
										IRF_Counter = 0;
										printf("\nPBF CRUISER HAS BEEN SUNK!\n");
									}
								}
								
								else {

									if(PBF_Grid[IRF.y][IRF.x] == '4') {

										(PBF.battleship)--;
										printf("\nPBF GOT HIT\n");
										PBF_hit = true;

										if(PBF.battleship == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.right = false;
											IRF.left = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}
										
									else {

										(PBF.carrier)--;
										printf("\nPBF GOT HIT\n");
										PBF_hit = true;

										if(PBF.carrier == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.right = false;
											IRF.left = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						IRF_VisibleGrid[IRF.y][IRF.x] = 'X';
						PBF_Turn = false;

						printf("\nIRF WENT\n");
						displayGrid(IRF_VisibleGrid);

						if(PBF.submarine == 0 && PBF.destroyer == 0 && PBF.cruiser == 0 && PBF.battleship == 0 && PBF.carrier == 0) {

						PBF_Wins = false;
						GameOver = true;
						}
					}
				}
			}

			printf("\nIRF WENT\n");
			displayGrid(IRF_VisibleGrid);
		}
	}

	if(PBF_Wins == true) 

		printf("\nPBF IS VICTORIOUS!\n");

	else

		printf("\nIRF IS VICTORIOUS!\n");

	printf("\nPlay again?\n");
	printf("1) Yes\n");
	printf("2) No\n");
	printf("Choice [1-2]: ");
	scanf("%d", choice);

	if(*choice == 1) 
		return true;

	else
		return false;
}

// The function displays the main menu.

void MainMenu(int* choice) {

	printf("< BATTLE FLEET GAME >\n\n");
	printf("Game Modes\n\n");
	printf("1) Player Vs Player\n");
	printf("2) Player Vs Computer\n");
	printf("3) Computer Vs Computer\n");
	printf("4) Exit Game\n\n");
	printf("Choose [1-3]: ");
	scanf("%d", choice);

	return;
}

// This function is used to deploy a submarine on a grid.

void deploySubmarine(char grid[10][10]) {

	int x = 0;
	int y = 0;
	int lastx = 0;
	int lasty = 0;

	printf("<DEPLOYING SUBMARINE>\n\n");
	printf("Enter First Coordinate [x,y]: ");
	scanf("%d,%d", &x, &y);

	while(check_Bounds(x,y) || check_Conflict(x,y,grid)) {

		fflush(stdin);
		printf("\nInvalid Coordinate...\n");
		printf("Enter First Coordinate [x,y]: ");
		scanf("%d,%d", &x, &y);
	}

	grid[y][x] = '1';

	lastx = x;
	lasty = y;

	displayGrid(grid);

	printf("\n\n");

	printf("Enter Second Coordinate [x,y]: ");
	scanf("%d,%d", &x, &y);

	while((x != lastx && y != lasty) || (x == lastx && (y != lasty + 1 && y != lasty - 1)) || (y == lasty && (x != lastx + 1 && x != lastx - 1)) || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

		fflush(stdin);
		printf("\nInvalid Coordinate...\n");
		printf("Enter Second Coordinate [x,y]: ");
		scanf("%d,%d", &x, &y);
	}
				
	grid[y][x] = '1';

	printf("<SUBMARINE DEPLOYED>\n\n");			

	displayGrid(grid);

	return;
}

// This function is used to deploy a destroyer on a grid.

void deployDestroyer(char grid[10][10]) {

	int x = 0;
	int y = 0;
	int lastx = 0;
	int lasty = 0;
	int initialx = 0;
	int initialy = 0;

	printf("\n<DEPLOYING DESTROYER>\n\n");
	printf("Enter First Coordinate [x,y]: ");
	scanf("%d,%d", &x, &y);

	while(check_Bounds(x,y) || check_Conflict(x,y,grid)) {

		fflush(stdin);
		printf("\nInvalid Coordinate...\n");
		printf("Enter First Coordinate [x,y]: ");
		scanf("%d,%d", &x, &y);
	}

	grid[y][x] = '2';

	lastx = x;
	lasty = y;

	displayGrid(grid);

	printf("\n\n");

	printf("Enter Second Coordinate [x,y]: ");
	scanf("%d,%d", &x, &y);

	while((x != lastx && y != lasty) || (x == lastx && (y != lasty + 1 && y != lasty - 1)) || (y == lasty && (x != lastx + 1 && x != lastx - 1)) || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

		fflush(stdin);
		printf("\nInvalid Coordinate...\n");
		printf("Enter Second Coordinate [x,y]: ");
		scanf("%d,%d", &x, &y);
	}
				

	grid[y][x] = '2';
	displayGrid(grid);

	if(x == lastx) {

		initialy = lasty;
		lasty = y;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialy > lasty) {

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				else {

					initialx = lastx;
					lastx = x;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialx > lastx) {

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				grid[y][x] = '2';

				printf("<DESTROYER DEPLOYED>\n\n");		

				displayGrid(grid);

				return;
			}

// This function is used to deploy a cruiser on a grid.

void deployCruiser(char grid[10][10]) {

	int x = 0;
	int y = 0;
	int lastx = 0;
	int lasty = 0;
	int initialx = 0;
	int initialy = 0;

				printf("\n<DEPLOYING CRUISER>\n\n");
				printf("Enter First Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

				while(check_Bounds(x,y) || check_Conflict(x,y,grid)) {

					fflush(stdin);
					printf("\nInvalid Coordinate...\n");
					printf("Enter First Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);
				}

				grid[y][x] = '3';
				lastx = x;
				lasty = y;

				displayGrid(grid);

				printf("\n\n");

				printf("Enter Second Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

					while((x != lastx && y != lasty) || (x == lastx && (y != lasty + 1 && y != lasty - 1)) || (y == lasty && (x != lastx + 1 && x != lastx - 1)) || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

						fflush(stdin);
						printf("\nInvalid Coordinate...\n");
						printf("Enter Second Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);
					}
				

				grid[y][x] = '3';
				displayGrid(grid);

				if(x == lastx) {

					initialy = lasty;
					lasty = y;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialy > lasty) {

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				else {

					initialx = lastx;
					lastx = x;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialx > lastx) {

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
		
					else {

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Second Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				grid[y][x] = '3';

				printf("<CRUISER DEPLOYED>\n\n");		

				displayGrid(grid);

				return;
}

// This function is used to deploy a battleship on a grid.

void deployBattleship(char grid[10][10]) {

	int x = 0;
	int y = 0;
	int lastx = 0;
	int lasty = 0;
	int initialx = 0;
	int initialy = 0;

				printf("\n<DEPLOYING BATTLESHIP>\n\n");
				printf("Enter First Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

				while(check_Bounds(x,y) || check_Conflict(x,y,grid)) {

					fflush(stdin);
					printf("\nInvalid Coordinate...\n");
					printf("Enter First Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);
				}

				grid[y][x] = '4';
				lastx = x;
				lasty = y;

				displayGrid(grid);

				printf("\n\n");

				printf("Enter Second Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

					while((x != lastx && y != lasty) || (x == lastx && (y != lasty + 1 && y != lasty - 1)) || (y == lasty && (x != lastx + 1 && x != lastx - 1)) || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

						fflush(stdin);
						printf("\nInvalid Coordinate...\n");
						printf("Enter Second Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);
					}
				

				grid[y][x] = '4';
				displayGrid(grid);

				if(x == lastx) {

					initialy = lasty;
					lasty = y;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialy > lasty) {

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '4';
						displayGrid(grid);

						if(y == initialy + 1) 

							initialy++;

						else

							lasty--;

					printf("Enter Fourth Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						if(y == initialy - 1) 

							initialy--;

						else

							lasty++;

						grid[y][x] = '4';
						displayGrid(grid);

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						
						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				else {

					initialx = lastx;
					lastx = x;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialx > lastx) {

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '4';
						displayGrid(grid);

						if(x == initialx + 1) 

							initialx++;

						else

							lastx--;

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '4';
						displayGrid(grid);

						if(x == initialx - 1) 

							initialx--;

						else

							lastx++;

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}


				printf("<BATTLESHIP DEPLOYED>\n\n");		
				grid[y][x] = '4';
				displayGrid(grid);

				return;
			}   

// This function is used to deploy a carrier on a grid.

void deployCarrier(char grid[10][10]) {

	int x = 0;
	int y = 0;
	int lastx = 0;
	int lasty = 0;
	int initialx = 0;
	int initialy = 0;

				printf("\n<DEPLOYING CARRIER>\n\n");
				printf("Enter First Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

				while(check_Bounds(x,y) || check_Conflict(x,y,grid)) {

					fflush(stdin);
					printf("\nInvalid Coordinate...\n");
					printf("Enter First Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);
				}

				grid[y][x] = '5';
				lastx = x;
				lasty = y;

				displayGrid(grid);

				printf("\n\n");

				printf("Enter Second Coordinate [x,y]: ");
				scanf("%d,%d", &x, &y);

					while((x != lastx && y != lasty) || (x == lastx && (y != lasty + 1 && y != lasty - 1)) || (y == lasty && (x != lastx + 1 && x != lastx - 1)) || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

						fflush(stdin);
						printf("\nInvalid Coordinate...\n");
						printf("Enter Second Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);
					}
				

				grid[y][x] = '5';
				displayGrid(grid);

				if(x == lastx) {

					initialy = lasty;
					lasty = y;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialy > lasty) {

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '5';
						displayGrid(grid);

						if(y == initialy + 1) 

							initialy++;

						else

							lasty--;

					printf("Enter Fourth Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '5';
						displayGrid(grid);

						if(y == initialy + 1) 

							initialy++;

						else

							lasty--;

					printf("Enter Fifth Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

						while(y != initialy + 1 && y != lasty - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fifth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						if(y == initialy - 1) 

							initialy--;

						else

							lasty++;

						grid[y][x] = '5';
						displayGrid(grid);

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						
						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

							grid[y][x] = '5';
							displayGrid(grid);

							if(y == initialy - 1) 

								initialy--;

							else

								lasty++;

							printf("Enter Fifth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);

						while(y != initialy - 1 && y != lasty + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fifth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}

				else {

					initialx = lastx;
					lastx = x;

					printf("Enter Third Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

					if(initialx > lastx) {

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '5';
						displayGrid(grid);
						
						if(x == initialx + 1) 
							
							initialx++;

						else

							lastx--;

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

							grid[y][x] = '5';
							displayGrid(grid);

							if(x == initialx + 1) 

								initialx++;

							else

								lastx--;

					printf("Enter Fifth Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

						while(x != initialx + 1 && x != lastx - 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fifth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}

					else {

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Third Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

						grid[y][x] = '5';
						displayGrid(grid);

						if(x == initialx - 1) 

							initialx--;

						else

							lastx++;

						printf("Enter Fourth Coordinate [x,y]: ");
						scanf("%d,%d", &x, &y);

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fourth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}

							grid[y][x] = '5';
							displayGrid(grid);

							if(x == initialx - 1) 

								initialx--;

							else

								lastx++;

					printf("Enter Fifth Coordinate [x,y]: ");
					scanf("%d,%d", &x, &y);

						while(x != initialx - 1 && x != lastx + 1 || check_Bounds(x,y) || check_Conflict(x,y,grid)) {

							fflush(stdin);
							printf("\nInvalid Coordinate...\n");
							printf("Enter Fifth Coordinate [x,y]: ");
							scanf("%d,%d", &x, &y);
						}
					}
				}


				printf("<CARRIER DEPLOYED>\n\n");		
				grid[y][x] = '5';
				displayGrid(grid);

				return;
			}

// This function is used to deploy a fleet of ships.

void deployFleet(char grid[10][10]) {

	deploySubmarine(grid);

	deployDestroyer(grid);

	deployCruiser(grid);

	deployBattleship(grid);

	deployCarrier(grid);

	return;
}

// Driver code for the Player VS Player Mode.
// Returns true if the player decides to play again.

bool PVPMode(int* choice) {

	char PBF_Grid[10][10] = {NULL};
	char PBF_VisibleGrid[10][10] = {NULL};
	char IRF_Grid[10][10] = {NULL};
	char IRF_VisibleGrid[10][10] = {NULL};
	bool PBF_Turn = true;
	bool PBF_Wins = true;
	bool GameOver = false;

	FLEET PBF = {2,3,3,4,5};
	FLEET IRF = {2,3,3,4,5};

	printf("\nPBF... Strategically deploy your fleet for combat!\n\n");

	deployFleet(PBF_Grid);

	printf("\n\nPBF Fleet Deployed!\n\n");

	printf("IRF... Strategically deploy your fleet for combat!\n\n");

	deployFleet(IRF_Grid);

	printf("\n\nIRF Fleet Deployed!\n\n");

	printf("Both sides have deployed their fleets...!\n");
	printf("Let the battle begin!\n\n");

	while(GameOver == false) {

		if(PBF_Turn == true) {

			printf("\nPBF's Turn...\n\n");
			
			displayGrid(PBF_VisibleGrid);

			printf("\n\nFire at a coordinate [x,y] > ");
			scanf("%d,%d", &PBF.x, &PBF.y);

			while(check_Bounds(PBF.x, PBF.y) || check_Conflict(PBF.x, PBF.y, PBF_VisibleGrid)) {

				fflush(stdin);
				printf("Invalid coordinate...\n");
				printf("\n\nFire at a coordinate [x,y] > ");
				scanf("%d,%d", &PBF.x, &PBF.y);
			}

			if(IRF_Grid[PBF.y][PBF.x] == NULL) {

				PBF_VisibleGrid[PBF.y][PBF.x] = 'O';
				PBF_Turn = false;
				printf("\n\nYou Missed!\n\n");
			}

			else {

				if(IRF_Grid[PBF.y][PBF.x] == '1') {

							(IRF.submarine)--;	

							if(IRF.submarine == 0) {

								printf("\nIRF SUBMARINE HAS BEEN SUNK!\n");
							}

						}
	
						else {		

							if(IRF_Grid[PBF.y][PBF.x] == '2') {

								(IRF.destroyer)--;

								if(IRF.destroyer == 0) {
	
									printf("\nIRF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(IRF_Grid[PBF.y][PBF.x] == '3') {
							
									(IRF.cruiser)--;

									if(IRF.cruiser == 0) {

										printf("\nIRF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(IRF_Grid[PBF.y][PBF.x] == '4') {

										(IRF.battleship)--;

										if(IRF.battleship == 0) {
	
											printf("\nIRF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(IRF.carrier)--;
										
										if(IRF.carrier == 0) {

											printf("\nIRF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						PBF_VisibleGrid[PBF.y][PBF.x] = 'X';

						printf("\nIRF GOT HIT!\n");

						displayMsg();

						if(IRF.submarine == 0 && IRF.destroyer == 0 && IRF.cruiser == 0 && IRF.battleship == 0 && IRF.carrier == 0) {

							PBF_Wins = true;
							GameOver = true;
						}	
			}
		}

		else {

			printf("\nIRF's Turn...\n\n");
			
			displayGrid(IRF_VisibleGrid);

			printf("\n\nFire at a coordinate [x,y] > ");
			scanf("%d,%d", &IRF.x, &IRF.y);

			while(check_Bounds(IRF.x, IRF.y) || check_Conflict(IRF.x, IRF.y, IRF_VisibleGrid)) {

				fflush(stdin);
				printf("Invalid coordinate...\n");
				printf("\n\nFire at a coordinate [x,y] > ");
				scanf("%d,%d", &IRF.x, &IRF.y);
			}

			if(PBF_Grid[IRF.y][IRF.x] == NULL) {

				IRF_VisibleGrid[IRF.y][IRF.x] = 'O';
				PBF_Turn = true;
				printf("\n\nYou Missed!\n\n");
			}

			else {

				if(PBF_Grid[IRF.y][IRF.x] == '1') {

							(PBF.submarine)--;	
							
							if(PBF.submarine == 0) {

								printf("\nPBF SUBMARINE HAS BEEN SUNK!\n");
							}

						}
	
						else {		

							if(PBF_Grid[IRF.y][IRF.x] == '2') {

								(PBF.destroyer)--;

								if(PBF.destroyer == 0) {
	
									printf("\nPBF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(PBF_Grid[IRF.y][IRF.x] == '3') {
							
									(PBF.cruiser)--;

									if(PBF.cruiser == 0) {

										printf("\nPBF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(PBF_Grid[IRF.y][IRF.x] == '4') {

										(PBF.battleship)--;
										printf("\nPBF GOT HIT!\n");

										if(PBF.battleship == 0) {
	
											printf("\nPBF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(PBF.carrier)--;

										if(PBF.carrier == 0) {

											printf("\nPBF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						IRF_VisibleGrid[IRF.y][IRF.x] = 'X';

						printf("\nPBF GOT HIT!\n");

						displayMsg();

						if(PBF.submarine == 0 && PBF.destroyer == 0 && PBF.cruiser == 0 && PBF.battleship == 0 && PBF.carrier == 0) {

							PBF_Wins = false;
							GameOver = true;
						}
				}
			}
		}

	if(PBF_Wins == true) 

		printf("\n\nPBF IS VICTORIOUS!\n");

	else

		printf("\n\nIRF IS VICTORIOUS!\n");

	printf("\n\nPlay again?\n");
	printf("1) Yes\n");
	printf("2) No\n");
	printf("Choice [1-2]: ");
	scanf("%d", choice);

	if(*choice == 1) 
		return true;

	else
		return false;
	} 

// When Player VS Computer mode is chosen, this function allows the user to choose a difficulty.
// Returns true if the player decides to play again.

bool PVCMode(int* choice) {

	printf("\n\n<Choose Difficulty>\n");
	printf("1) Easy\n");
	printf("2) Medium\n");
	printf("3) Hard\n\n");
	printf("Choose [1-3]: ");
	scanf("%d", choice);

	while(*choice > 3 || *choice < 1) {

		printf("\nInvalid Choice...");
		printf("\n\n<Choose Difficulty>\n");
		printf("1) Easy\n");
		printf("2) Medium\n");
		printf("3) Hard\n\n");
		printf("Choose [1-3]: ");
		scanf("%d", choice);
	}

	switch(*choice) {

		case 1 : {

			return easyMode(choice);
		
		}

		case 2 : {

			return intermediateMode(choice);

		}

		case 3 : {

			return hardMode(choice);

		}

	}
}

// Driver code for the PVC Easy Mode.
// Returns true if the player decides to play again.

bool easyMode(int* choice) {

	char PBF_Grid[10][10] = {NULL};
	char PBF_VisibleGrid[10][10] = {NULL};
	char IRF_Grid[10][10] = {NULL};
	char IRF_VisibleGrid[10][10] = {NULL};
	bool PBF_Turn = true;
	bool PBF_Wins = true;
	bool GameOver = false;

	FLEET PBF = {2,3,3,4,5};
	FLEET IRF = {2,3,3,4,5};

	printf("\nPBF... Strategically deploy your fleet for combat!\n\n");

	deployFleet(PBF_Grid);

	printf("\n\nPBF Fleet Deployed!\n\n");

	srand(time(NULL));

	generateGrid(IRF_Grid);

	printf("Both sides have deployed their fleets...!\n");
	printf("Let the battle begin!\n\n");

	while(GameOver == false) {

		if(PBF_Turn == true) {

			printf("\nPBF's Turn...\n\n");
			
			displayGrid(PBF_VisibleGrid);

			printf("\n\nFire at a coordinate [x,y] > ");
			scanf("%d,%d", &PBF.x, &PBF.y);

			while(check_Bounds(PBF.x, PBF.y) || check_Conflict(PBF.x, PBF.y, PBF_VisibleGrid)) {

				fflush(stdin);
				printf("Invalid coordinate...\n");
				printf("\n\nFire at a coordinate [x,y] > ");
				scanf("%d,%d", &PBF.x, &PBF.y);
			}

			if(IRF_Grid[PBF.y][PBF.x] == NULL) {

				PBF_VisibleGrid[PBF.y][PBF.x] = 'O';
				PBF_Turn = false;
				printf("\n\nYou Missed!\n\n");
			}

			else {

				if(IRF_Grid[PBF.y][PBF.x] == '1') {

							(IRF.submarine)--;	
							
							if(IRF.submarine == 0) {

								printf("\nIRF SUBMARINE HAS BEEN SUNK!\n");
							}

						}
	
						else {		

							if(IRF_Grid[PBF.y][PBF.x] == '2') {

								(IRF.destroyer)--;

								if(IRF.destroyer == 0) {
	
									printf("\nIRF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(IRF_Grid[PBF.y][PBF.x] == '3') {
							
									(IRF.cruiser)--;

									if(IRF.cruiser == 0) {

										printf("\nIRF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(IRF_Grid[PBF.y][PBF.x] == '4') {

										(IRF.battleship)--;

										if(IRF.battleship == 0) {
	
											printf("\nIRF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(IRF.carrier)--;

										if(IRF.carrier == 0) {

											printf("\nIRF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						PBF_VisibleGrid[PBF.y][PBF.x] = 'X';

						printf("\nIRF GOT HIT!\n");

						displayMsg();

						if(IRF.submarine == 0 && IRF.destroyer == 0 && IRF.cruiser == 0 && IRF.battleship == 0 && IRF.carrier == 0) {

							PBF_Wins = true;
							GameOver = true;
						}	
			}
		}

		else {

			IRF.x = rand() % 10;
			IRF.y = rand() % 10;

			if(IRF_VisibleGrid[IRF.y][IRF.x] != 'O' && IRF_VisibleGrid[IRF.y][IRF.x] != 'X') {

				if(PBF_Grid[IRF.y][IRF.x] == NULL) {

					IRF_VisibleGrid[IRF.y][IRF.x] = 'O';
					PBF_Turn = true;
					printf("\nIRF Missed!\n");
				}

				else {

					if(PBF_Grid[IRF.y][IRF.x] == '1') {

							(PBF.submarine)--;	

							if(PBF.submarine == 0) {

								printf("\nPBF SUBMARINE HAS BEEN SUNK!\n");
							}
						}

						else {		

							if(PBF_Grid[IRF.y][IRF.x] == '2') {

								(PBF.destroyer)--;

								if(PBF.destroyer == 0) {

									printf("\nPBF DESTROYER HAS BEEN SUNK!\n");
								}
							}

							else {

								if(PBF_Grid[IRF.y][IRF.x] == '3') {
							
									(PBF.cruiser)--;

									if(PBF.cruiser == 0) {

										printf("\nPBF CRUISER HAS BEEN SUNK!\n");
									}
								}
								
								else {

									if(PBF_Grid[IRF.y][IRF.x] == '4') {

										(PBF.battleship)--;

										if(PBF.battleship == 0) {

											printf("\nPBF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}
										
									else {

										(PBF.carrier)--;
				
										if(PBF.carrier == 0) {

											printf("\nPBF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						IRF_VisibleGrid[IRF.y][IRF.x] = 'X';
						printf("\nPBF GOT HIT\n");
						printf("\nIRF WENT\n");
						displayGrid(IRF_VisibleGrid);

						if(PBF.submarine == 0 && PBF.destroyer == 0 && PBF.cruiser == 0 && PBF.battleship == 0 && PBF.carrier == 0) {

						PBF_Wins = false;
						GameOver = true;
						}
				}
			}
		}
	}

	if(PBF_Wins == true) 

		printf("\nPBF IS VICTORIOUS!\n");

	else

		printf("\nIRF IS VICTORIOUS!\n");

	printf("\nPlay again?\n");
	printf("1) Yes\n");
	printf("2) No\n");
	printf("Choice [1-2]: ");
	scanf("%d", choice);

	if(*choice == 1) 
		return true;

	else
		return false;
}

// Driver code for the PVC Intermediate Mode.
// Returns true if the player decides to play again.

bool intermediateMode(int* choice) {

	char PBF_Grid[10][10] = {NULL};
	char PBF_VisibleGrid[10][10] = {NULL};
	char IRF_Grid[10][10] = {NULL};
	char IRF_VisibleGrid[10][10] = {NULL};
	bool PBF_Turn = true;
	bool PBF_Wins = true;
	bool GameOver = false;
	bool PBF_hit = false;
	bool PBF_ShipFound = false;
	int IRF_Counter = 0;

	FLEET PBF = {2,3,3,4,5};
	FLEET IRF = {2,3,3,4,5};

	printf("\nPBF... Strategically deploy your fleet for combat!\n\n");

	deployFleet(PBF_Grid);

	printf("\n\nPBF Fleet Deployed!\n\n");

	srand(time(NULL));

	generateGrid(IRF_Grid);

	printf("Both sides have deployed their fleets...!\n");
	printf("Let the battle begin!\n\n");

	while(GameOver == false) {

		if(PBF_Turn == true) {

			while(PBF_Turn == true) {

				printf("\nPBF's Turn...\n\n");
			
				displayGrid(PBF_VisibleGrid);

				printf("\n\nFire at a coordinate [x,y] > ");
				scanf("%d,%d", &PBF.x, &PBF.y);

				while(check_Bounds(PBF.x, PBF.y) || check_Conflict(PBF.x, PBF.y, PBF_VisibleGrid)) {

					fflush(stdin);
					printf("Invalid coordinate...\n");
					printf("\n\nFire at a coordinate [x,y] > ");
					scanf("%d,%d", &PBF.x, &PBF.y);
				}

				if(IRF_Grid[PBF.y][PBF.x] == NULL) {

					PBF_VisibleGrid[PBF.y][PBF.x] = 'O';
					PBF_Turn = false;
					printf("\n\nYou Missed!\n\n");
				}

				else {

					if(IRF_Grid[PBF.y][PBF.x] == '1') {

						(IRF.submarine)--;	
							
						if(IRF.submarine == 0) {

							printf("\nIRF SUBMARINE HAS BEEN SUNK!\n");
						}

					}
	
						else {		

							if(IRF_Grid[PBF.y][PBF.x] == '2') {

								(IRF.destroyer)--;

								if(IRF.destroyer == 0) {
	
									printf("\nIRF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(IRF_Grid[PBF.y][PBF.x] == '3') {
							
									(IRF.cruiser)--;

									if(IRF.cruiser == 0) {

										printf("\nIRF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(IRF_Grid[PBF.y][PBF.x] == '4') {

										(IRF.battleship)--;

										if(IRF.battleship == 0) {
	
											printf("\nIRF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(IRF.carrier)--;

										if(IRF.carrier == 0) {

											printf("\nIRF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						PBF_VisibleGrid[PBF.y][PBF.x] = 'X';

						printf("\nIRF GOT HIT!\n");

						displayMsg();

						if(IRF.submarine == 0 && IRF.destroyer == 0 && IRF.cruiser == 0 && IRF.battleship == 0 && IRF.carrier == 0) {

							PBF_Wins = true;
							GameOver = true;
						}	
					}
				}
		}

		else {

			while(PBF_Turn == false && GameOver == false) {

				if(PBF_hit == false && PBF_ShipFound == false)  {

					IRF.x = rand() % 10;
					IRF.y = rand() % 10;
				}

				if(IRF_VisibleGrid[IRF.y][IRF.x] == 'O' || IRF_VisibleGrid[IRF.y][IRF.x] == 'X')

					PBF_Turn = false;


				if(PBF_ShipFound == true) {

					attackShipMedium(IRF_VisibleGrid, PBF_Grid, &IRF, &IRF_Counter, &PBF_ShipFound, &PBF_hit);
				}

				if(PBF_hit == true && PBF_ShipFound == false) {

					findShipMedium(IRF_VisibleGrid, PBF_Grid, &PBF_ShipFound, &IRF);
					PBF_Turn = true;

				}	

				if(PBF_Grid[IRF.y][IRF.x] == NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'O') {

					IRF_VisibleGrid[IRF.y][IRF.x] = 'O';
					PBF_Turn = true;
					printf("\nIRF Missed!\n");
				}

				else {

					if(PBF_Grid[IRF.y][IRF.x] != NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'X') {

						if(PBF_Grid[IRF.y][IRF.x] == '1') {

							(PBF.submarine)--;	
							PBF_hit = true;

							if(PBF.submarine == 0) {

								PBF_ShipFound = false;
								PBF_hit = false;
								IRF.up = false;
								IRF.down = false;
								IRF.counter = 0;
								IRF_Counter = 0;
								printf("\nPBF SUBMARINE HAS BEEN SUNK!\n");
							}
						}

						else {		

							if(PBF_Grid[IRF.y][IRF.x] == '2') {

								(PBF.destroyer)--;
								PBF_hit = true;

								if(PBF.destroyer == 0) {

									PBF_ShipFound = false;
									PBF_hit = false;
									IRF.up = false;
									IRF.down = false;
									IRF.counter = 0;
									IRF_Counter = 0;
									printf("\nPBF DESTROYER HAS BEEN SUNK!\n");
								}
							}

							else {

								if(PBF_Grid[IRF.y][IRF.x] == '3') {
							
									(PBF.cruiser)--;
									PBF_hit = true;

									if(PBF.cruiser == 0) {

										PBF_ShipFound = false;
										PBF_hit = false;
										IRF.up = false;
										IRF.down = false;
										IRF.counter = 0;
										IRF_Counter = 0;
										printf("\nPBF CRUISER HAS BEEN SUNK!\n");
									}
								}
								
								else {

									if(PBF_Grid[IRF.y][IRF.x] == '4') {

										(PBF.battleship)--;
										PBF_hit = true;

										if(PBF.battleship == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}
										
									else {

										(PBF.carrier)--;
										PBF_hit = true;

										if(PBF.carrier == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.right = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						IRF_VisibleGrid[IRF.y][IRF.x] = 'X';
						PBF_Turn = false;

						printf("\nIRF WENT\n");
						printf("\nPBF GOT HIT\n");
						displayGrid(IRF_VisibleGrid);

						if(PBF.submarine == 0 && PBF.destroyer == 0 && PBF.cruiser == 0 && PBF.battleship == 0 && PBF.carrier == 0) {

						PBF_Wins = false;
						GameOver = true;
						}
					}
				}
			}

			printf("\nIRF WENT\n");
			displayGrid(IRF_VisibleGrid);
		}
	}

	if(PBF_Wins == true) 

		printf("\nPBF IS VICTORIOUS!\n");

	else

		printf("\nIRF IS VICTORIOUS!\n");

	printf("\nPlay again?\n");
	printf("1) Yes\n");
	printf("2) No\n");
	printf("Choice [1-2]: ");
	scanf("%d", choice);

	if(*choice == 1) 
		return true;

	else
		return false;
}

// Driver code for the PVC Hard Mode.
// Returns true if the player decides to play again.

bool hardMode(int* choice) {

	char PBF_Grid[10][10] = {NULL};
	char PBF_VisibleGrid[10][10] = {NULL};
	char IRF_Grid[10][10] = {NULL};
	char IRF_VisibleGrid[10][10] = {NULL};
	bool PBF_Turn = true;
	bool PBF_Wins = true;
	bool GameOver = false;
	bool PBF_hit = false;
	bool PBF_ShipFound = false;
	int IRF_Counter = 0;

	FLEET PBF = {2,3,3,4,5};
	FLEET IRF = {2,3,3,4,5};

	printf("\nPBF... Strategically deploy your fleet for combat!\n\n");

	deployFleet(PBF_Grid);

	printf("\n\nPBF Fleet Deployed!\n\n");

	srand(time(NULL));

	generateGrid(IRF_Grid);

	printf("Both sides have deployed their fleets...!\n");
	printf("Let the battle begin!\n\n");

	while(GameOver == false) {

		if(PBF_Turn == true && GameOver == false) {

			while(PBF_Turn == true && GameOver == false) {

				printf("\nPBF's Turn...\n\n");
			
				displayGrid(PBF_VisibleGrid);

				printf("\n\nFire at a coordinate [x,y] > ");
				scanf("%d,%d", &PBF.x, &PBF.y);

				while(check_Bounds(PBF.x, PBF.y) || check_Conflict(PBF.x, PBF.y, PBF_VisibleGrid)) {

					fflush(stdin);
					printf("Invalid coordinate...\n");
					printf("\n\nFire at a coordinate [x,y] > ");
					scanf("%d,%d", &PBF.x, &PBF.y);
				}

				if(IRF_Grid[PBF.y][PBF.x] == NULL) {

					PBF_VisibleGrid[PBF.y][PBF.x] = 'O';
					PBF_Turn = false;
					printf("\n\nYou Missed!\n\n");
				}

				else {

					if(IRF_Grid[PBF.y][PBF.x] == '1') {

						(IRF.submarine)--;	
							
						if(IRF.submarine == 0) {

							printf("\nIRF SUBMARINE HAS BEEN SUNK!\n");
						}

					}
	
						else {		

							if(IRF_Grid[PBF.y][PBF.x] == '2') {

								(IRF.destroyer)--;

								if(IRF.destroyer == 0) {
	
									printf("\nIRF DESTROYER HAS BEEN SUNK!\n");
								}
							}

								else {
	
								if(IRF_Grid[PBF.y][PBF.x] == '3') {
							
									(IRF.cruiser)--;

									if(IRF.cruiser == 0) {

										printf("\nIRF CRUISER HAS BEEN SUNK!\n");
									}
								}
									
								else {

									if(IRF_Grid[PBF.y][PBF.x] == '4') {

										(IRF.battleship)--;

										if(IRF.battleship == 0) {
	
											printf("\nIRF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}

									else {

										(IRF.carrier)--;

										if(IRF.carrier == 0) {

											printf("\nIRF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						PBF_VisibleGrid[PBF.y][PBF.x] = 'X';

						printf("\nIRF GOT HIT!\n");

						displayMsg();

						if(IRF.submarine == 0 && IRF.destroyer == 0 && IRF.cruiser == 0 && IRF.battleship == 0 && IRF.carrier == 0) {

							PBF_Wins = true;
							GameOver = true;
						}	
					}
				}
		}

		else {

			while(PBF_Turn == false && GameOver == false) {

				if(PBF_hit == false && PBF_ShipFound == false)  {

					IRF.x = rand() % 10;
					IRF.y = rand() % 10;
				}

				if(IRF_VisibleGrid[IRF.y][IRF.x] == 'O' || IRF_VisibleGrid[IRF.y][IRF.x] == 'X')

					PBF_Turn = false;


				if(PBF_ShipFound == true) {

					attackShip(IRF_VisibleGrid, PBF_Grid, &IRF, &IRF_Counter, &PBF_ShipFound);
				}

				if(PBF_hit == true && PBF_ShipFound == false) {

					findShip(IRF_VisibleGrid, PBF_Grid, &PBF_ShipFound, &IRF);
					PBF_Turn = true;

				}	

				if(PBF_Grid[IRF.y][IRF.x] == NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'O') {

					IRF_VisibleGrid[IRF.y][IRF.x] = 'O';
					PBF_Turn = true;
					printf("\nIRF WENT\n");
					printf("\nIRF Missed!\n");
				}

				else {

					if(PBF_Grid[IRF.y][IRF.x] != NULL && IRF_VisibleGrid[IRF.y][IRF.x] != 'X') {

						if(PBF_Grid[IRF.y][IRF.x] == '1') {

							(PBF.submarine)--;	
							PBF_hit = true;

							if(PBF.submarine == 0) {

								PBF_ShipFound = false;
								PBF_hit = false;
								IRF.up = false;
								IRF.down = false;
								IRF.right = false;
								IRF.left = false;
								IRF.counter = 0;
								IRF_Counter = 0;
								printf("\nPBF SUBMARINE HAS BEEN SUNK!\n");
							}
						}

						else {		

							if(PBF_Grid[IRF.y][IRF.x] == '2') {

								(PBF.destroyer)--;
								PBF_hit = true;

								if(PBF.destroyer == 0) {

									PBF_ShipFound = false;
									PBF_hit = false;
									IRF.up = false;
									IRF.down = false;
									IRF.right = false;
									IRF.left = false;
									IRF.counter = 0;
									IRF_Counter = 0;
									printf("\nPBF DESTROYER HAS BEEN SUNK!\n");
								}
							}

							else {

								if(PBF_Grid[IRF.y][IRF.x] == '3') {
							
									(PBF.cruiser)--;
									PBF_hit = true;

									if(PBF.cruiser == 0) {

										PBF_ShipFound = false;
										PBF_hit = false;
										IRF.up = false;
										IRF.down = false;
										IRF.right = false;
										IRF.left = false;
										IRF.counter = 0;
										IRF_Counter = 0;
										printf("\nPBF CRUISER HAS BEEN SUNK!\n");
									}
								}
								
								else {

									if(PBF_Grid[IRF.y][IRF.x] == '4') {

										(PBF.battleship)--;
										PBF_hit = true;

										if(PBF.battleship == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.right = false;
											IRF.left = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF BATTLESHIP HAS BEEN SUNK!\n");
										}
									}
										
									else {

										(PBF.carrier)--;;
										PBF_hit = true;

										if(PBF.carrier == 0) {

											PBF_ShipFound = false;
											PBF_hit = false;
											IRF.up = false;
											IRF.down = false;
											IRF.right = false;
											IRF.left = false;
											IRF.counter = 0;
											IRF_Counter = 0;
											printf("\nPBF CARRIER HAS BEEN SUNK!\n");
										}
									}
								}
							}
						}

						IRF_VisibleGrid[IRF.y][IRF.x] = 'X';
						PBF_Turn = false;

						printf("\nIRF WENT\n");
						printf("\nPBF GOT HIT\n");

						if(PBF.submarine == 0 && PBF.destroyer == 0 && PBF.cruiser == 0 && PBF.battleship == 0 && PBF.carrier == 0) {

						PBF_Wins = false;
						GameOver = true;
						}
					}
				}
			}

			displayGrid(IRF_VisibleGrid);
		}
	}

	if(PBF_Wins == true) 

		printf("\nPBF IS VICTORIOUS!\n");

	else

		printf("\nIRF IS VICTORIOUS!\n");

	printf("\nPlay again?\n");
	printf("1) Yes\n");
	printf("2) No\n");
	printf("Choice [1-2]: ");
	scanf("%d", choice);

	if(*choice == 1) 
		return true;

	else
		return false;
}

// This is a stripped down version of the findShip() function.
// It is used in intermediate mode and  only tries to find a horizontal match.
	
void findShipMedium(char grid[10][10], char enemyGrid[10][10], bool* shipFound, FLEET* myFleet) {

			int horizontalDirection = 0;
		
			horizontalDirection = rand() % 2;

			if(horizontalDirection == 0) {

				if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

					if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

						(myFleet->x)++;
						(myFleet->counter)++;
						(myFleet->right) = true;
						(*shipFound) = true;
					}

					else

						grid[myFleet->y][(myFleet->x) + 1] = 'O';


				}

				else {

					if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

						if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {
							
							(myFleet->x)--;
							(myFleet->counter)++;	
							(myFleet->left) = true;
							(*shipFound) = true;
						}

						else 

							grid[myFleet->y][(myFleet->x) - 1] = 'O';
			
					}
				}
			}
					
			else {

				if((myFleet->x) - 1 >= 0 && grid[myFleet->y][(myFleet->x) - 1] != 'X' && grid[myFleet->y][(myFleet->x) - 1] != 'O') {

					if(enemyGrid[myFleet->y][(myFleet->x) - 1] != NULL) {

						(myFleet->x)--;
						(myFleet->counter)++;
						(myFleet->left) = true;
						(*shipFound) = true;
					}

					else

						grid[myFleet->y][(myFleet->x) - 1] = 'O';


				}

				else {

					if((myFleet->x) + 1 < 10 && grid[myFleet->y][(myFleet->x) + 1] != 'X' && grid[myFleet->y][(myFleet->x) + 1] != 'O') {

						if(enemyGrid[myFleet->y][(myFleet->x) + 1] != NULL) {

							(myFleet->x)++;
							(myFleet->counter)++;
							(myFleet->right) = true;
							(*shipFound) = true;
						}

						else

							grid[myFleet->y][(myFleet->x) + 1] = 'O';

					}
				}
			}
		

		return;
}

// This is a stripped down version of the attackShip() function.
// It is used in intermediate mode and continues to fire on a ship horizontally.

void attackShipMedium(char grid[10][10], char enemyGrid[10][10], FLEET* myFleet, int* counter, bool* shipFound, bool* PBF_Hit) {

	if(*counter < 2) {

		if((myFleet->right) == true) {

			if(grid[myFleet->y][(myFleet->x) + 1] == 'O' || grid[myFleet->y][myFleet->x] == 'O' || grid[myFleet->y][(myFleet->x) + 1] == 'X' || (myFleet->x) + 1 > 9) {

				myFleet->x = myFleet->x - ++(myFleet->counter);
				myFleet->left = true;
				myFleet->right = false;
				(*counter)++;
			}

			else {

				myFleet->x = ++(myFleet->x);
				(myFleet->counter)++;
			}
		}

		else {

			if((myFleet->left) == true) {

				if(grid[myFleet->y][(myFleet->x) - 1] == 'O' || grid[myFleet->y][myFleet->x] == 'O' || grid[myFleet->y][(myFleet->x) - 1] == 'X' || (myFleet->x) - 1 < 0) {

					myFleet->x = myFleet->x + ++(myFleet->counter);
					myFleet->right = true;
					myFleet->left = false;
					(*counter)++;
				}

				else {

					myFleet->x = --(myFleet->x);
					(myFleet->counter)++;
				}
			}
		}
	}

	else {

		*shipFound = false;
		*PBF_Hit = false;
		*counter = 0;

	}

	return;
}

// This function displays a random message when the player hits a ship.

void displayMsg(void) {

	int msg = 0;

	msg = rand() % 4;

	switch(msg) {

	case 0 : printf("\n\nDirect Hit!\n\n");
		break;

	case 1 : printf("\n\nHit her again!\n\n");
		break;

	case 2 : printf("\n\nGreat Shot!\n\n");
		break;

	case 3 : printf("\n\nGo for the kill!\n\n");
		break;

	}

	return;
}
	
	
	
	






